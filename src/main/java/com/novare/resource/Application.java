package com.novare.resource;

import com.novare.resource.model.Department;
import com.novare.resource.model.Employee;
import com.novare.resource.model.Position;
import com.novare.resource.repository.DepartmentRepository;
import com.novare.resource.repository.EmployeeRepository;
import com.novare.resource.repository.PositionRepository;
import com.novare.resource.repository.ProjectRepository;
import com.novare.resource.spring.config.WebConfig;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.data.rest.webmvc.config.RepositoryRestMvcConfiguration;

import java.util.Date;
import java.util.List;

/**
 * Created by gardnertorres on 11/01/2017.
 */
@Configuration
@ComponentScan
@EnableJpaRepositories
@Import(RepositoryRestMvcConfiguration.class)
@EnableAutoConfiguration
public class Application {

    private static final Logger log = LoggerFactory.getLogger(Application.class);

    public static void main(String arg[]) {
        SpringApplication.run(Application.class);
    }

    @Bean
    public CommandLineRunner demo(PositionRepository positionRepository,
                                  DepartmentRepository departmentRepository,
                                  EmployeeRepository employeeRepository,
                                  ProjectRepository projectRepository) {

        Date startDate = new Date();
        Date completionDate = new Date();
        Date hiredDate = new Date();
        Date resignedDate = new Date();

        return (args) -> {

//            // save a couple of position
//            positionRepository.save(new Position("AS", "Associate Engineer"));
//            positionRepository.save(new Position("SE1", "Software Engineer 1"));
//            positionRepository.save(new Position("SE2", "Software Engineer 2"));
//            positionRepository.save(new Position("SSE", "Senior Software Engineer"));
//            positionRepository.save(new Position("PSE", "Principal Software Engineer"));
//            positionRepository.save(new Position("TM", "Technical Manager"));

            // save a couple of project
//            projectRepository.save(new Project("FFM", "Force Field Management", 5,startDate, completionDate, 5));
//            projectRepository.save(new Project("CONSENT", "Consent", 5,startDate, completionDate, 5));
//            projectRepository.save(new Project("ECMS-WEBAPP", "Enterprise Capability Management System", 5,startDate, completionDate, 5));
//            projectRepository.save(new Project("THEA", "Globe-Thea Data File Proc", 5,startDate, completionDate, 5));
//            projectRepository.save(new Project("BIG BIRD", "Project Big Bird", 5,startDate, completionDate, 5));

//            // save a couple of department
//            departmentRepository.save(new Department((long) 001,"PMO", "Project Management Office"));
//            departmentRepository.save(new Department((long) 002,"SEG", "Software Engineering Group"));
//            departmentRepository.save(new Department((long) 003,"SDG", "Service Delivery Group"));


//            // fetch all position
//            log.info("Position found with findAll():");
//            log.info("-------------------------------");
//            for (Position position : positionRepository.findAll()) {
//                log.info(position.toString());
//            }
//            log.info("");
//
//            // fetch an individual position by ID
//            Position position = positionRepository.findOne(1L);
//            log.info("Department found with findOne(1L):");
//            log.info("--------------------------------");
//            log.info(position.toString());
//            log.info("");
//
            // fetch department by department name
//            log.info("Department found with findByDepartmentName('Senior Software Engineer'):");
//            log.info("--------------------------------------------");
//            Position position =  new Position();
//            for (Position pos : positionRepository.findByPositionName("Senior Software Engineer")) {
//                log.info(pos.toString());
//                position = pos;
//            }
//            log.info("");
//
//            // fetch all department
//            log.info("Department found with findAll():");
//            log.info("-------------------------------");
//            for (Department department : departmentRepository.findAll()) {
//                log.info(department.toString());
//            }
//            log.info("");
//
//            // fetch an individual department by ID
//            Department department = departmentRepository.findOne(1L);
//            log.info("Department found with findOne(1L):");
//            log.info("--------------------------------");
//            log.info(department.toString());
//            log.info("");
//
            // fetch department by department name
//            log.info("Department found with findByDepartmentName('Software Engineering Group'):");
//            log.info("--------------------------------------------");
//            Department department = new Department();
//            for (Department dept : departmentRepository.findByDepartmentName("Software Engineering Group")) {
//                log.info(dept.toString());
//                department = dept;
//            }
//            log.info("");
//
//            // save a couple of employee
//            employeeRepository.save(new Employee((long) 001,"Gardner", "Lorenzo", "Torres", hiredDate, resignedDate, 3, 50, department, position));
//            employeeRepository.save(new Employee((long) 002,"Roland John", " ", "Vocalan", hiredDate, resignedDate, 1, 0, department, position));
//            employeeRepository.save(new Employee((long) 003,"Alvin", "Cris", "Tabontabon", hiredDate, resignedDate, 1, 80, department, position));
//            employeeRepository.save(new Employee((long) 004,"Edmilyn", "Joy", "Domingo", hiredDate, resignedDate, 1, 50, department, position));
//            employeeRepository.save(new Employee((long) 005,"Glen", "Carlo", "Estonilo", hiredDate, resignedDate, 1, 10, department, position));

//            // fetch all employee
//            log.info("Customers found with findAll():");
//            log.info("-------------------------------");
//            for (Employee employee : employeeRepository.findAll()) {
//                log.info(employee.toString());
//            }
//            log.info("");
//
//            // fetch an individual employee by ID
//            Employee employee = employeeRepository.findOne(1L);
//            log.info("Customer found with findOne(1L):");
//            log.info("--------------------------------");
//            log.info(employee.toString());
//            log.info("");
//
//            // fetch employee by last name
//            log.info("Customer found with findByLastName('Bauer'):");
//            log.info("--------------------------------------------");
//            for (Employee bauer : employeeRepository.findByLastName("Bauer")) {
//                log.info(bauer.toString());
//            }
//            log.info("");
        };
    }
}