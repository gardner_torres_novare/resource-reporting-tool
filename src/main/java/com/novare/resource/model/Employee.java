package com.novare.resource.model;

import javax.persistence.*;
import java.util.Date;

/**
 * Created by gardnertorres on 11/01/2017.
 */
@Entity
@Table(name = "employee")
public class Employee {

    @Id
    private Long employee_id;

    @OneToOne
    @JoinColumn(name = "department_id")
    private Department department;

    @OneToOne
    @JoinColumn(name = "position_id")
    private Position position;

    private String firstName;
    private String middleName;
    private String lastName;
    private Date date_hired;
    private Date date_resigned;
    private int level;
    private int allocation;

    protected Employee () {}

    public Employee(Long employee_id, String firstName, String middleName,
                    String lastName, Date date_hired, Date date_resigned,
                    int level, int allocation, Department department, Position position) {

        this.employee_id = employee_id;
        this.firstName = firstName;
        this.middleName = middleName;
        this.lastName = lastName;
        this.date_hired = date_hired;
        this.date_resigned = date_resigned;
        this.level = level;
        this.allocation = allocation;
        this.department = department;
        this.position = position;
    }

    public Long getEmployee_id() {
        return employee_id;
    }

    public void setEmployee_id(Long employee_id) {
        this.employee_id = employee_id;
    }

    public Department getDepartment() {
        return department;
    }

    public void setDepartment(Department department) {
        this.department = department;
    }

    public Position getPosition() {
        return position;
    }

    public void setPosition(Position position) {
        this.position = position;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getMiddleName() {
        return middleName;
    }

    public void setMiddleName(String middleName) {
        this.middleName = middleName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public Date getDate_hired() {
        return date_hired;
    }

    public void setDate_hired(Date date_hired) {
        this.date_hired = date_hired;
    }

    public Date getDate_resigned() {
        return date_resigned;
    }

    public void setDate_resigned(Date date_resigned) {
        this.date_resigned = date_resigned;
    }

    public int getLevel() {
        return level;
    }

    public void setLevel(int level) {
        this.level = level;
    }

    public int getAllocation() {
        return allocation;
    }

    public void setAllocation(int allocation) {
        this.allocation = allocation;
    }

    @Override
    public String toString() {
        return String.format(
                "Employee[id=%d, firstName='%s', lastName='%s']",
                employee_id, firstName, middleName, lastName);
    }
}
