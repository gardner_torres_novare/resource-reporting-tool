package com.novare.resource.model;

import javax.persistence.*;

/**
 * Created by gardnertorres on 12/01/2017.
 */
@Entity
@Table(name = "department")
public class Department {

    @Id
    @GeneratedValue(strategy= GenerationType.AUTO)
    private Long department_id;
    private String departmentCode;
    private String departmentName;

    public Department() {}

    public Department(Long department_id, String departmentCode, String departmentName){
        this.department_id = department_id;
        this.departmentCode = departmentCode;
        this.departmentName = departmentName;
    }

    public Long getDepartment_id() {
        return department_id;
    }

    public void setDepartment_id(Long department_id) {
        this.department_id = department_id;
    }

    public String getDepartmentCode() {
        return departmentCode;
    }

    public void setDepartmentCode(String departmentCode) {
        this.departmentCode = departmentCode;
    }

    public String getDepartmentName() {
        return departmentName;
    }

    public void setDepartmentName(String departmentName) {
        this.departmentName = departmentName;
    }

    //    @Override
//    public String toString() {
//        return String.format(
//                "Employee[id=%d, firstName='%s', lastName='%s']",
//                employee_id, firstName, middleName, lastName);
//    }
}
