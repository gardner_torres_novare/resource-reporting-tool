package com.novare.resource.model;

import javax.persistence.*;
import java.util.Date;

/**
 * Created by gardnertorres on 12/01/2017.
 */
@Entity
@Table(name = "project")
public class Project {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long project_id;
    private String projectCode;
    private String projectName;
    private int requiredNumberResource;
    private Date startDate;
    private Date completionDate;
    private int totalResourceAlloc;

    protected Project() {
    }

    public Project(String projectCode, String projectName, int requiredNumberResource, Date startDate, Date completionDate, int totalResourceAlloc) {
        this.projectCode = projectCode;
        this.projectName = projectName;
        this.requiredNumberResource = requiredNumberResource;
        this.startDate = startDate;
        this.completionDate = completionDate;
        this.totalResourceAlloc = totalResourceAlloc;
    }

    public Long getProject_id() {
        return project_id;
    }

    public void setProject_id(Long project_id) {
        this.project_id = project_id;
    }

    public String getProjectCode() {
        return projectCode;
    }

    public void setProjectCode(String projectCode) {
        this.projectCode = projectCode;
    }

    public String getProjectName() {
        return projectName;
    }

    public void setProjectName(String projectName) {
        this.projectName = projectName;
    }

    public int getRequiredNumberResource() {
        return requiredNumberResource;
    }

    public void setRequiredNumberResource(int requiredNumberResource) {
        this.requiredNumberResource = requiredNumberResource;
    }

    public Date getStartDate() {
        return startDate;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    public Date getCompletionDate() {
        return completionDate;
    }

    public void setCompletionDate(Date completionDate) {
        this.completionDate = completionDate;
    }

    public int getTotalResourceAlloc() {
        return totalResourceAlloc;
    }

    public void setTotalResourceAlloc(int totalResourceAlloc) {
        this.totalResourceAlloc = totalResourceAlloc;
    }

//    @Override
//    public String toString() {
//        return String.format(
//                "Employee[id=%d, firstName='%s', lastName='%s']",
//                employee_id, firstName, middleName, lastName);

}