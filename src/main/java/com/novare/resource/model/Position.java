package com.novare.resource.model;

import javax.persistence.*;

/**
 * Created by gardnertorres on 12/01/2017.
 */
@Entity
@Table (name = "position")
public class Position {

    @Id
    @GeneratedValue(strategy= GenerationType.AUTO)
    private Long position_id;
    private String positionCode;
    private String positionName;


    public Position() {}

    public Position(String positionCode, String positionName){
        this.positionCode = positionCode;
        this.positionName = positionName;
    }

    public Long getPosition_id() {
        return position_id;
    }

    public void setPosition_id(Long position_id) {
        this.position_id = position_id;
    }

    public String getPositionCode() {
        return positionCode;
    }

    public void setPositionCode(String positionCode) {
        this.positionCode = positionCode;
    }

    public String getPositionName() {
        return positionName;
    }

    public void setPositionName(String positionName) {
        this.positionName = positionName;
    }

    //    @Override
//    public String toString() {
//        return String.format(
//                "Employee[id=%d, firstName='%s', lastName='%s']",
//                employee_id, firstName, middleName, lastName);
//    }
}
