package com.novare.resource.model;

import javax.persistence.*;
import java.util.Date;

/**
 * Created by gardnertorres on 12/01/2017.
 */
@Entity
@Table (name = "employee_project")
public class EmployeeProject {

    @Id
    @GeneratedValue(strategy= GenerationType.AUTO)
    private Long allocation_id;

    @ManyToOne
    @JoinColumn(name = "employee_id")
    private Employee employee;

    @ManyToOne
    @JoinColumn(name = "project_id")
    private Project project;

    private int percentageAllocation;
    private Date startDate;
    private Date endDate;
    private int numberOfDays;

    public Long getAllocation_id() {
        return allocation_id;
    }

    public void setAllocation_id(Long allocation_id) {
        this.allocation_id = allocation_id;
    }

    public Employee getEmployee() {
        return employee;
    }

    public void setEmployee(Employee employee) {
        this.employee = employee;
    }

    public Project getProject() {
        return project;
    }

    public void setProject(Project project) {
        this.project = project;
    }

    public int getPercentageAllocation() {
        return percentageAllocation;
    }

    public void setPercentageAllocation(int percentageAllocation) {
        this.percentageAllocation = percentageAllocation;
    }

    public Date getStartDate() {
        return startDate;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    public Date getEndDate() {
        return endDate;
    }

    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }

    public int getNumberOfDays() {
        return numberOfDays;
    }

    public void setNumberOfDays(int numberOfDays) {
        this.numberOfDays = numberOfDays;
    }

//    @Override
//    public String toString() {
//        return String.format(
//                "Employee[id=%d, firstName='%s', lastName='%s']",
//                employee_id, firstName, middleName, lastName);

}
