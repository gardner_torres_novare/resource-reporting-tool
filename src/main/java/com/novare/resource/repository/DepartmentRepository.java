package com.novare.resource.repository;

import com.novare.resource.model.Department;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

/**
 * Created by gardnertorres on 12/01/2017.
 */
public interface DepartmentRepository extends CrudRepository<Department, Long> {

    List<Department> findByDepartmentName(String departmentName);
    List<Department> findAll();
}
