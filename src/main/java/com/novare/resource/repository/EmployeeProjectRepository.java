package com.novare.resource.repository;

import com.novare.resource.model.EmployeeProject;
import org.springframework.data.repository.CrudRepository;

import java.util.Date;
import java.util.List;

/**
 * Created by gardnertorres on 13/01/2017.
 */
public interface EmployeeProjectRepository extends CrudRepository<EmployeeProject, Long> {

    List<EmployeeProject> findByStartDate(Date startDate);
}
