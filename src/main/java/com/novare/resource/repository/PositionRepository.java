package com.novare.resource.repository;

import com.novare.resource.model.Position;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

/**
 * Created by gardnertorres on 12/01/2017.
 */
public interface PositionRepository extends CrudRepository<Position, Long> {

    List<Position> findByPositionName(String name);
}
