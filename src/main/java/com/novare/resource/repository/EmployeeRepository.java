package com.novare.resource.repository;

import com.novare.resource.model.Employee;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * Created by gardnertorres on 11/01/2017.
 */
@Component
public interface EmployeeRepository extends CrudRepository<Employee, Long> {

    List<Employee> findByLastName(String lastName);
}
