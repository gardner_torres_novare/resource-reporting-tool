package com.novare.resource.repository;

import com.novare.resource.model.Project;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

/**
 * Created by gardnertorres on 13/01/2017.
 */
public interface ProjectRepository extends CrudRepository<Project, Long> {

    List<Project> findByProjectName(String name);
}
