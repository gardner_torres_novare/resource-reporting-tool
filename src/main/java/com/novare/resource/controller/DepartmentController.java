package com.novare.resource.controller;

import com.novare.resource.model.Department;
import com.novare.resource.service.DepartmentService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.util.UriComponentsBuilder;

import java.util.List;

/**
 * Created by gardnertorres on 17/01/2017.
 */
@Controller
public class DepartmentController {

    private static final Logger log = LoggerFactory.getLogger(DepartmentController.class);

    @Autowired
    private DepartmentService departmentService;

    @RequestMapping("/departmentList")
    public ResponseEntity<List<Department>> departmentHome(UriComponentsBuilder builder) {
        List<Department> departmentList = departmentService.findAllDepartments();
        HttpHeaders headers = new HttpHeaders();
        headers.setLocation(builder.path("/departmentForm").build().toUri());
        if(departmentList.isEmpty()){
            return new ResponseEntity<List<Department>>(headers, HttpStatus.NO_CONTENT);//You many decide to return HttpStatus.NOT_FOUND
        }
        log.info("Retrieved DeptList: " + departmentList.size());
        return new ResponseEntity<List<Department>>(departmentList, HttpStatus.OK);
    }

    @RequestMapping(value = "/createDepartment", method = RequestMethod.POST)
    public ResponseEntity<Department> createDepartment(@RequestBody Department department, UriComponentsBuilder builder) {
        departmentService.saveDepartment(department);
        HttpHeaders headers = new HttpHeaders();
        headers.setLocation(builder.path("/departmentForm").build().toUri());
        return new ResponseEntity<>(headers, HttpStatus.CREATED);
    }
}
