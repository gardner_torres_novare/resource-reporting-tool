package com.novare.resource.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * Created by gardnertorres on 16/01/2017.
 */
@Controller
public class MainTemplateController {

    private static final Logger log = LoggerFactory.getLogger(MainTemplateController.class);

    @RequestMapping("/")
    public String home() {
        return "base/layout";
    }

    @RequestMapping("/resource/dashboard")
      public String viewDashboard() {
        return "dashboard";
    }

    @RequestMapping("/resource/reports")
    public String viewReports() {
        return "reports";
    }

    @RequestMapping("/resource/projects")
    public String viewProjects() {
        return "projects";
    }

    @RequestMapping("/resource/employees")
    public String viewEmployees() {
        return "employees";
    }

    @RequestMapping("/resource/departments")
    public String viewDepartments() {
        return "departments";
    }

    @RequestMapping("/resource/positions")
    public String viewPositions() {
        return "positions";
    }

    @RequestMapping("/resource/departmentForm")
     public String addNewDepartment() {
        return "departmentForm";
    }

}
