package com.novare.resource.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * Created by gardnertorres on 17/01/2017.
 */
@Controller
public class ProjectController {

    @RequestMapping("/projectList")
    public String home() {
        return "projects";
    }
}
