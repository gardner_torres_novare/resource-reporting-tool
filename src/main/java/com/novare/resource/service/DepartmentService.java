package com.novare.resource.service;

import com.novare.resource.model.Department;

import java.util.List;

/**
 * Created by gardnertorres on 20/01/2017.
 */
public interface DepartmentService {

    List<Department> findAllDepartments();
    void saveDepartment(Department department);
}
