var app = angular.module('resourceTool', ['ngRoute']);

app.config(['$routeProvider', function($routeProvider) {
	$routeProvider
		.when('/resource/dashboard', {
			templateUrl: 'resource/dashboard',
			controller: 'myCntrl'
		})
		.when('/resource/reports', {
			templateUrl: 'resource/reports',
			controller: 'myCntrl'
		})
		.when('/resource/projects', {
			templateUrl: 'resource/projects',
			controller: 'myCntrl'
		})
		.when('/resource/employees', {
			templateUrl: 'resource/employees',
			controller: 'myCntrl'
		})
		.when('/resource/departments', {
			templateUrl: 'resource/departments',
			controller: 'myCntrl'
		})
		.when('/resource/positions', {
			templateUrl: 'resource/positions',
			controller: 'myCntrl'
		})
		.when('/resource/departmentForm', {
			templateUrl: 'resource/departmentForm',
			controller: 'myCntrl'
		})
		.otherwise({redirectTo:'/resource/dashboard'});

}]);

app.controller('myCntrl', function($scope, $http, $window, appService) {

	var self = this;
	self.dept = {departmentCode:'',departmentName:''};
	self.deptList = [];

	$scope.submitForm = function(){
		self.dept.departmentCode = $scope.dept_code;
		self.dept.departmentName = $scope.dept_name;
		createDepartment(self.dept);
	}

	//fetchAllDepartment();

	function fetchAllDepartment() {
		appService.fetchAllDept()
			.then(
			function(data) {
				self.deptList = data;
				console.log(self.deptList.size);
			},
			function(errResponse){
				console.error('Error while fetching Departments');
			}
		)
	}

	function createDepartment(dept) {
		appService.createDepartment(dept)
			.then(
			function(d) {},
			function(errResponse) {
				console.error('Error While creating Department');
			}
		)
	}

	$scope.home = function() {
		//window.alert("View Home Clicked");
	};
	$scope.viewReport = function() {
		//window.alert("View Report Clicked");
	};
	$scope.viewProjects = function() {
		//window.alert("View Projects Clicked");
	};
	$scope.viewEmployees = function() {
		//window.alert("View Employees Clicked");
	};
	$scope.showDeptForm = function() {
		//$http.get("http://localhost:8080/departmentForm").
		//	success(function(data) {
		//		alert("success");
			//	$window.location.href("http://localhost:8080/departmentForm");
			//	//$window.$location = $window;
			//	//$scope.Data = data;
			//}).
			//error(function(data){
			//	alert("failure");
			//});
	};
});

app.factory('appService', function($http, $q) {

	var SERVICE_URI = "http://localhost:8080";
	var factory = {
		fetchAllDept: fetchAllDept,
		createDepartment: createDepartment
	};

	function fetchAllDept() {
		var deferred = $q.defer();
		$http.get(SERVICE_URI+"/departmentList")
			.then(
			function (response) {
				console.log('Departments received!!');
				deferred.resolve(response.data);
			},
			function(errResponse){
				console.error('Error while fetching Departments');
				deferred.reject(errResponse);
			}
		);
		return deferred.promise;

	}

	function createDepartment(department) {
		window.alert("createDepartment: "+department.departmentCode+" - "+ department.departmentName)
		var deferred = $q.defer();
		$http.post(SERVICE_URI+"/createDepartment", department)
			.then(
			function (response) {
				deferred.resolve(response.data);
			},
			function(errResponse){
				console.error('Error while creating Department');
				deferred.reject(errResponse);
			}
		);
		return deferred.promise;
	}

	return factory;
});